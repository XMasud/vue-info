import Vue from 'vue'
import App from './App.vue'

window.$ = window.jQuery = require('jquery')
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

new Vue({
  el: '#app',
  render: h => h(App)
})
